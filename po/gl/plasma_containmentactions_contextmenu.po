# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marce Villarino <mvillarino@gmail.com>, 2009, 2011.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2014.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-17 03:28+0000\n"
"PO-Revision-Date: 2019-08-19 20:29+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: menu.cpp:98
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "Mostrar KRunner"

#: menu.cpp:103
#, kde-format
msgid "Open Terminal"
msgstr ""

#: menu.cpp:107
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "Bloquear a pantalla"

#: menu.cpp:116
#, fuzzy, kde-format
#| msgctxt "plasma_containmentactions_contextmenu"
#| msgid "Leave..."
msgctxt "plasma_containmentactions_contextmenu"
msgid "Leave…"
msgstr "Saír…"

#: menu.cpp:125
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Display Settings…"
msgstr ""

#: menu.cpp:290
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "Configurar o complemento de menú contextual"

#: menu.cpp:300
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "(outras accións)"

#: menu.cpp:303
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "Accións do fondo de escritorio"

#: menu.cpp:307
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "(separador)"

#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Run Command..."
#~ msgstr "Executar unha orde…"
