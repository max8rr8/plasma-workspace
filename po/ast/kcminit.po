# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-26 00:47+0000\n"
"PO-Revision-Date: 2023-05-01 23:56+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Softastur"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alministradores@softastur.org"

#: main.cpp:170
#, kde-format
msgid "KCMInit"
msgstr ""

#: main.cpp:172
#, kde-format
msgid "KCMInit - runs startup initialization for Control Modules."
msgstr ""

#: main.cpp:178
#, kde-format
msgid "List modules that are run at startup"
msgstr ""

#: main.cpp:179
#, kde-format
msgid "Configuration module to run"
msgstr ""
