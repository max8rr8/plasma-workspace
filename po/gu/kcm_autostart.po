# translation of kcm_autostart.po to Gujarati
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sweta Kothari <swkothar@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-01 02:34+0000\n"
"PO-Revision-Date: 2009-01-05 16:02+0530\n"
"Last-Translator: Sweta Kothari <swkothar@redhat.com>\n"
"Language-Team: Gujarati\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#: autostartmodel.cpp:320
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr ""

#: autostartmodel.cpp:323
#, kde-format
msgid "\"%1\" does not exist."
msgstr ""

#: autostartmodel.cpp:326
#, kde-format
msgid "\"%1\" is not a file."
msgstr ""

#: autostartmodel.cpp:329
#, kde-format
msgid "\"%1\" is not readable."
msgstr ""

#: ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:95
#, fuzzy, kde-format
#| msgid "&Properties"
msgid "Properties"
msgstr "ગુણધર્મો (&P)"

#: ui/main.qml:101
#, fuzzy, kde-format
#| msgid "&Remove"
msgid "Remove"
msgstr "દૂર કરો (&R)"

#: ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:118
#, fuzzy, kde-format
#| msgid "Pre-KDE startup"
msgid "Pre-startup Scripts"
msgstr "પહેલાની KDE શરૂ કરો"

#: ui/main.qml:121
#, kde-format
msgid "Logout Scripts"
msgstr ""

#: ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""

#: ui/main.qml:145
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "સ્ક્રિપ્ટ ને ઉમેરો..."

#: ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr ""

#: ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr ""

#: ui/main.qml:203
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Login Script…"
msgstr "સ્ક્રિપ્ટ ને ઉમેરો..."

#: ui/main.qml:209
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Logout Script…"
msgstr "સ્ક્રિપ્ટ ને ઉમેરો..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "શ્ર્વેતા કોઠારી"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "swkothar@redhat.com"

#, fuzzy
#~| msgid "KDE Autostart Manager Control Panel Module"
#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "KDE સ્વયં શરૂ થતુ વ્યવસ્થાપક નિયંત્રણ પેનલ મોડ્યુલ"

#, fuzzy
#~| msgid "(c) 2006-2007-2008 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "(c) 2006-2007-2008 સ્વયં શરૂ થતુ વ્યવસ્થાપક જૂથ"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "સુસ્થાપિત કરનાર"

#, fuzzy
#~| msgid "Advanced"
#~ msgid "Add..."
#~ msgstr "ઉન્નત્તિ થયેલ"

#, fuzzy
#~| msgid "Shell script:"
#~ msgid "Shell script path:"
#~ msgstr "Shell સ્ક્રિપ્ટ:"

#~ msgid "Create as symlink"
#~ msgstr "symlink તરીકે બનાવો"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "KDE માં ફક્ત સ્વયં શરૂ કરો"

#~ msgid "Name"
#~ msgstr "નામ"

#~ msgid "Command"
#~ msgstr "આદેશ"

#~ msgid "Status"
#~ msgstr "સ્થિતિ"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "પર ચલાવો"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "KDE સ્વયં શરૂ થતો વ્યસ્થાપક"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "સક્રિય થયેલ છે"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "નિષ્ક્રિય થયેલ છે"

#~ msgid "Desktop File"
#~ msgstr "ડેસ્કટોપ ફાઇલ"

#~ msgid "Script File"
#~ msgstr "સ્ક્રિપ્ટ ફાઇલ"

#~ msgid "Add Program..."
#~ msgstr "પ્રક્રિયાને ઉમેરો..."

#~ msgid "Startup"
#~ msgstr "શરૂ કરો"

#, fuzzy
#~| msgid ""
#~| "KDE only reads files with sh extensions for setting up the environment."
#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr "KDE ફક્ત પર્યાવરણ સુયોજિત કરવા માટે sh એક્સટેન્શનો સાથે ફાઇલો ને વાંચે છે."

#~ msgid "Shutdown"
#~ msgstr "બંધ કરો"
