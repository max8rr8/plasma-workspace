# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-01 02:34+0000\n"
"PO-Revision-Date: 2023-05-03 22:38+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: locationrunner.cpp:32
#, kde-format
msgid ""
"Finds local directories and files, network locations and Internet sites with "
"paths matching :q:."
msgstr ""

#: locationrunner.cpp:58
#, kde-format
msgid "Open %1"
msgstr ""

#: locationrunner.cpp:77 locationrunner.cpp:81
#, kde-format
msgid "Launch with %1"
msgstr ""

#: locationrunner.cpp:87
#, kde-format
msgid "Go to %1"
msgstr ""

#: locationrunner.cpp:95
#, kde-format
msgid "Send email to %1"
msgstr ""
