# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Edward Wornar <edi.werner@gmx.de>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-29 02:05+0000\n"
"PO-Revision-Date: 2021-11-18 21:06+0100\n"
"Last-Translator: Edward Wornar <edi.werner@gmx.de>\n"
"Language-Team: Upper Sorbian <kde-i18n-doc@kde.org>\n"
"Language: hsb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3;\n"

#: contents/ui/BarcodePage.qml:35
#, kde-format
msgid "Return to Clipboard"
msgstr "Wróćo do zapisnika"

#: contents/ui/BarcodePage.qml:53
#, kde-format
msgid "QR Code"
msgstr "QR Code"

#: contents/ui/BarcodePage.qml:54
#, kde-format
msgid "Data Matrix"
msgstr ""

#: contents/ui/BarcodePage.qml:55
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr ""

#: contents/ui/BarcodePage.qml:56
#, kde-format
msgid "Code 39"
msgstr ""

#: contents/ui/BarcodePage.qml:57
#, kde-format
msgid "Code 93"
msgstr ""

#: contents/ui/BarcodePage.qml:58
#, kde-format
msgid "Code 128"
msgstr ""

#: contents/ui/BarcodePage.qml:82
#, kde-format
msgid "Change the QR code type"
msgstr ""

#: contents/ui/BarcodePage.qml:137
#, kde-format
msgid "Creating QR code failed"
msgstr ""

#: contents/ui/BarcodePage.qml:148
#, kde-format
msgid ""
"There is not enough space to display the QR code. Try resizing this applet."
msgstr ""

#: contents/ui/clipboard.qml:28
#, kde-format
msgid "Clipboard Contents"
msgstr "Wobsah zapisnika"

#: contents/ui/clipboard.qml:29 contents/ui/Menu.qml:126
#, kde-format
msgid "Clipboard is empty"
msgstr "Zapisnik je prózdny"

#: contents/ui/clipboard.qml:57
#, kde-format
msgid "Configure Clipboard…"
msgstr "Zapisnik připrawić"

#: contents/ui/clipboard.qml:59
#, kde-format
msgid "Clear History"
msgstr "Předchadne wobsahi zabyć"

#: contents/ui/DelegateToolButtons.qml:23
#, kde-format
msgid "Invoke action"
msgstr "Akciju zwołać"

#: contents/ui/DelegateToolButtons.qml:38
#, kde-format
msgid "Show QR code"
msgstr ""

#: contents/ui/DelegateToolButtons.qml:54
#, kde-format
msgid "Edit contents"
msgstr "Wobsah wobdźěłać"

#: contents/ui/DelegateToolButtons.qml:68
#, kde-format
msgid "Remove from history"
msgstr "Zabyć"

#: contents/ui/EditPage.qml:86
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr ""

#: contents/ui/EditPage.qml:94
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: contents/ui/Menu.qml:126
#, kde-format
msgid "No matches"
msgstr "Ničo njenamakał"

#: contents/ui/UrlItemDelegate.qml:106
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr ""

#~ msgid "Clear history"
#~ msgstr "Předchadne wobsahi zabyć"

#~ msgid "Search…"
#~ msgstr "Pytać..."
