# translation of plasma_runner_baloosearchrunner.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2014, 2016.
# Mthw <jari_45@hotmail.com>, 2018.
# Matej Mrenica <matejm98mthw@gmail.com>, 2022, 2023.
# Ferdinand Galko <galko.ferdinand@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_baloosearchrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-09 01:04+0000\n"
"PO-Revision-Date: 2023-04-11 18:16+0200\n"
"Last-Translator: Ferdinand Galko <galko.ferdinand@gmail.com>\n"
"Language-Team: Slovak <opensuse-translation@opensuse.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: baloosearchrunner.cpp:65
#, kde-format
msgid "Open Containing Folder"
msgstr "Otvoriť priečinok s obsahom"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Audios"
msgstr "Zvuky"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Images"
msgstr "Obrázky"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Videos"
msgstr "Videá"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Spreadsheets"
msgstr "Tabuľky"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Presentations"
msgstr "Prezentácie"

#: baloosearchrunner.cpp:92
#, kde-format
msgid "Folders"
msgstr "Priečinky"

#: baloosearchrunner.cpp:93
#, kde-format
msgid "Documents"
msgstr "Dokumenty"

#: baloosearchrunner.cpp:94
#, kde-format
msgid "Archives"
msgstr "Archívy"

#: baloosearchrunner.cpp:95
#, kde-format
msgid "Texts"
msgstr "Texty"

#: baloosearchrunner.cpp:96
#, kde-format
msgid "Files"
msgstr "Súbory"

#~ msgid "Search through files, emails and contacts"
#~ msgstr "Hľadanie v súboroch, e-mailoch a kontaktoch"

#~ msgid "Email"
#~ msgstr "E-mail"
