# translation of kcm_users.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2020.
# Matej Mrenica <matejm98mthw@gmail.com>, 2020, 2021, 2022, 2023.
# Ferdinand Galko <galko.ferdinand@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kcm_users\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-09 02:11+0000\n"
"PO-Revision-Date: 2023-04-17 18:41+0200\n"
"Last-Translator: Ferdinand Galko <galko.ferdinand@gmail.com>\n"
"Language-Team: Slovak <opensuse-translation@opensuse.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: src/fingerprintmodel.cpp:151 src/fingerprintmodel.cpp:258
#, kde-format
msgid "No fingerprint device found."
msgstr "Nenašlo sa žiadne zariadenie na snímanie odtlačkov prstov."

#: src/fingerprintmodel.cpp:331
#, kde-format
msgid "Retry scanning your finger."
msgstr "Skúsiť znova naskenovať prst."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Potiahnutie prstom bolo príliš krátke. Skúste to znova."

#: src/fingerprintmodel.cpp:335
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Prst nie je vycentrovaný na čítačke. Skúste to znova."

#: src/fingerprintmodel.cpp:337
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Odstráňte prst z čítačky a skúste to znova."

#: src/fingerprintmodel.cpp:345
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Registrácia odtlačku prsta zlyhala."

#: src/fingerprintmodel.cpp:348
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Pre toto zariadenie nie je miesto, odstráňte ostatné odtlačky prstov na "
"pokračovanie."

#: src/fingerprintmodel.cpp:351
#, kde-format
msgid "The device was disconnected."
msgstr "Zariadenie bolo odpojené."

#: src/fingerprintmodel.cpp:356
#, kde-format
msgid "An unknown error has occurred."
msgstr "Vyskytla sa neznáma chyba."

#: src/ui/ChangePassword.qml:28 src/ui/UserDetailsPage.qml:159
#, kde-format
msgid "Change Password"
msgstr "Zmeniť heslo"

#: src/ui/ChangePassword.qml:33
#, kde-format
msgid "Set Password"
msgstr "Nastaviť heslo"

#: src/ui/ChangePassword.qml:56
#, kde-format
msgid "Password"
msgstr "Heslo"

#: src/ui/ChangePassword.qml:71
#, kde-format
msgid "Confirm password"
msgstr "Potvrďte heslo"

#: src/ui/ChangePassword.qml:90 src/ui/CreateUser.qml:68
#, kde-format
msgid "Passwords must match"
msgstr "Heslá sa musia zhodovať"

#: src/ui/ChangeWalletPassword.qml:16
#, kde-format
msgid "Change Wallet Password?"
msgstr "Zmeniť heslo peňaženky?"

#: src/ui/ChangeWalletPassword.qml:25
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Teraz, keď ste zmenili svoje prihlasovacie heslo, môžete tiež zmeniť heslo "
"svojej predvoleného KWallet, aby sa s ním zhodovalo."

#: src/ui/ChangeWalletPassword.qml:30
#, kde-format
msgid "What is KWallet?"
msgstr "Čo je KWallet?"

#: src/ui/ChangeWalletPassword.qml:40
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet je správca hesiel, ktorý ukladá vaše heslá pre bezdrôtové siete a "
"ďalšie šifrované zdroje. Je uzamknutý vlastným heslom, ktoré sa líši od "
"vášho prihlasovacieho hesla. Ak sa tieto dve heslá zhodujú, dá sa odomknúť "
"pri prihlásení automaticky, takže nemusíte zadávať heslo KWallet sami."

#: src/ui/ChangeWalletPassword.qml:56
#, kde-format
msgid "Change Wallet Password"
msgstr "Zmeniť heslo peňaženky"

#: src/ui/ChangeWalletPassword.qml:65
#, kde-format
msgid "Leave Unchanged"
msgstr "Nechať nezmenené"

#: src/ui/CreateUser.qml:15
#, kde-format
msgid "Create User"
msgstr "Vytvoriť používatelia"

#: src/ui/CreateUser.qml:33 src/ui/UserDetailsPage.qml:127
#, kde-format
msgid "Name:"
msgstr "Meno:"

#: src/ui/CreateUser.qml:37 src/ui/UserDetailsPage.qml:134
#, kde-format
msgid "Username:"
msgstr "Užívateľské meno:"

#: src/ui/CreateUser.qml:47 src/ui/UserDetailsPage.qml:142
#, kde-format
msgid "Standard"
msgstr "Štandardné"

#: src/ui/CreateUser.qml:48 src/ui/UserDetailsPage.qml:143
#, kde-format
msgid "Administrator"
msgstr "Administrátor"

#: src/ui/CreateUser.qml:51 src/ui/UserDetailsPage.qml:146
#, kde-format
msgid "Account type:"
msgstr "Typ účtu:"

#: src/ui/CreateUser.qml:56
#, kde-format
msgid "Password:"
msgstr "Heslo:"

#: src/ui/CreateUser.qml:61
#, kde-format
msgid "Confirm password:"
msgstr "Potvrďte heslo:"

#: src/ui/CreateUser.qml:78
#, kde-format
msgid "Create"
msgstr "Vytvoriť"

#: src/ui/FingerprintDialog.qml:57
#, kde-format
msgid "Configure Fingerprints"
msgstr "Nastaviť odtlačky prstov"

#: src/ui/FingerprintDialog.qml:67
#, kde-format
msgctxt "@action:button 'all' refers to fingerprints"
msgid "Clear All"
msgstr "Vyčistiť všetko"

#: src/ui/FingerprintDialog.qml:74
#, kde-format
msgid "Add"
msgstr "Pridať"

#: src/ui/FingerprintDialog.qml:83
#, kde-format
msgid "Cancel"
msgstr "Zrušiť"

#: src/ui/FingerprintDialog.qml:91
#, kde-format
msgid "Done"
msgstr "Hotovo"

#: src/ui/FingerprintDialog.qml:115
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Pridáva sa odtlačok"

#: src/ui/FingerprintDialog.qml:124
#, kde-format
msgid ""
"Please repeatedly press your right index finger on the fingerprint sensor."
msgstr "Opakovane priložte svoj pravý ukazovák na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:126
#, kde-format
msgid ""
"Please repeatedly press your right middle finger on the fingerprint sensor."
msgstr "Opakovane priložte svoj pravý prostredník na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:128
#, kde-format
msgid ""
"Please repeatedly press your right ring finger on the fingerprint sensor."
msgstr "Opakovane priložte svoj pravý prstenník na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:130
#, kde-format
msgid ""
"Please repeatedly press your right little finger on the fingerprint sensor."
msgstr "Opakovane priložte svoj pravý malíček na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:132
#, kde-format
msgid "Please repeatedly press your right thumb on the fingerprint sensor."
msgstr "Opakovane priložte svoj pravý palec na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:134
#, kde-format
msgid ""
"Please repeatedly press your left index finger on the fingerprint sensor."
msgstr "Opakovane priložte svoj ľavý ukazovák na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:136
#, kde-format
msgid ""
"Please repeatedly press your left middle finger on the fingerprint sensor."
msgstr "Opakovane priložte svoj ľavý prostredník na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:138
#, kde-format
msgid ""
"Please repeatedly press your left ring finger on the fingerprint sensor."
msgstr "Opakovane priložte svoj ľavý prstenník na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:140
#, kde-format
msgid ""
"Please repeatedly press your left little finger on the fingerprint sensor."
msgstr "Opakovane priložte svoj ľavý malíček na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:142
#, kde-format
msgid "Please repeatedly press your left thumb on the fingerprint sensor."
msgstr "Opakovane priložte svoj ľavý palec na snímač odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:146
#, kde-format
msgid ""
"Please repeatedly swipe your right index finger on the fingerprint sensor."
msgstr "Opakovane potiahnite pravým ukazovákom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:148
#, kde-format
msgid ""
"Please repeatedly swipe your right middle finger on the fingerprint sensor."
msgstr "Opakovane potiahnite pravým prostredníkom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:150
#, kde-format
msgid ""
"Please repeatedly swipe your right ring finger on the fingerprint sensor."
msgstr "Opakovane potiahnite pravým prstenníkom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:152
#, kde-format
msgid ""
"Please repeatedly swipe your right little finger on the fingerprint sensor."
msgstr "Opakovane potiahnite pravým malíčkom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:154
#, kde-format
msgid "Please repeatedly swipe your right thumb on the fingerprint sensor."
msgstr "Opakovane potiahnite pravým palcom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:156
#, kde-format
msgid ""
"Please repeatedly swipe your left index finger on the fingerprint sensor."
msgstr "Opakovane potiahnite ľavým ukazovákom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:158
#, kde-format
msgid ""
"Please repeatedly swipe your left middle finger on the fingerprint sensor."
msgstr "Opakovane potiahnite ľavým prostredníkom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:160
#, kde-format
msgid ""
"Please repeatedly swipe your left ring finger on the fingerprint sensor."
msgstr "Opakovane potiahnite ľavým prstenníkom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:162
#, kde-format
msgid ""
"Please repeatedly swipe your left little finger on the fingerprint sensor."
msgstr "Opakovane potiahnite ľavým malíčkom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:164
#, kde-format
msgid "Please repeatedly swipe your left thumb on the fingerprint sensor."
msgstr "Opakovane potiahnite ľavým palcom po snímači odtlačkov prstov."

#: src/ui/FingerprintDialog.qml:179
#, kde-format
msgid "Finger Enrolled"
msgstr "Odtlačok pridaný"

#: src/ui/FingerprintDialog.qml:209
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Vyberte prst na pridanie"

#: src/ui/FingerprintDialog.qml:326
#, kde-format
msgid "Re-enroll finger"
msgstr "Znovu pridať prst"

#: src/ui/FingerprintDialog.qml:333
#, kde-format
msgid "Delete fingerprint"
msgstr "Vymazať odtlačok prsta"

#: src/ui/FingerprintDialog.qml:342
#, kde-format
msgid "No fingerprints added"
msgstr "Žiadne odtlačky neboli pridané"

#: src/ui/main.qml:19
#, kde-format
msgid "Manage Users"
msgstr "Spravovať používateľov"

#: src/ui/main.qml:125
#, kde-format
msgid "Add New User"
msgstr "Pridať nového používateľa"

#: src/ui/PicturesSheet.qml:18
#, kde-format
msgctxt "@title"
msgid "Change Avatar"
msgstr "Zmeniť avatar"

#: src/ui/PicturesSheet.qml:22
#, kde-format
msgctxt "@item:intable"
msgid "It's Nothing"
msgstr "Nič to nie je"

#: src/ui/PicturesSheet.qml:23
#, kde-format
msgctxt "@item:intable"
msgid "Feisty Flamingo"
msgstr "Výbojný plameniak"

#: src/ui/PicturesSheet.qml:24
#, kde-format
msgctxt "@item:intable"
msgid "Dragon's Fruit"
msgstr "Pitaja"

#: src/ui/PicturesSheet.qml:25
#, kde-format
msgctxt "@item:intable"
msgid "Sweet Potato"
msgstr "Sladký zemiak"

#: src/ui/PicturesSheet.qml:26
#, kde-format
msgctxt "@item:intable"
msgid "Ambient Amber"
msgstr "Jantárová"

#: src/ui/PicturesSheet.qml:27
#, kde-format
msgctxt "@item:intable"
msgid "Sparkle Sunbeam"
msgstr "Ligotavý slnečný lúč"

#: src/ui/PicturesSheet.qml:28
#, kde-format
msgctxt "@item:intable"
msgid "Lemon-Lime"
msgstr "Limonáda"

#: src/ui/PicturesSheet.qml:29
#, kde-format
msgctxt "@item:intable"
msgid "Verdant Charm"
msgstr "Zelené čaro"

#: src/ui/PicturesSheet.qml:30
#, kde-format
msgctxt "@item:intable"
msgid "Mellow Meadow"
msgstr "Mäkká Lúka"

#: src/ui/PicturesSheet.qml:31
#, kde-format
msgctxt "@item:intable"
msgid "Tepid Teal"
msgstr "Vlažná sivozelená"

#: src/ui/PicturesSheet.qml:32
#, kde-format
msgctxt "@item:intable"
msgid "Plasma Blue"
msgstr "Plasmová modrá"

#: src/ui/PicturesSheet.qml:33
#, kde-format
msgctxt "@item:intable"
msgid "Pon Purple"
msgstr "Pon fialová"

#: src/ui/PicturesSheet.qml:34
#, kde-format
msgctxt "@item:intable"
msgid "Bajo Purple"
msgstr "Bajo fialová"

#: src/ui/PicturesSheet.qml:35
#, kde-format
msgctxt "@item:intable"
msgid "Burnt Charcoal"
msgstr "Spálené uhlie"

#: src/ui/PicturesSheet.qml:36
#, kde-format
msgctxt "@item:intable"
msgid "Paper Perfection"
msgstr "Dokonalosť papiera"

#: src/ui/PicturesSheet.qml:37
#, kde-format
msgctxt "@item:intable"
msgid "Cafétera Brown"
msgstr "Kávová hnedá"

#: src/ui/PicturesSheet.qml:38
#, kde-format
msgctxt "@item:intable"
msgid "Rich Hardwood"
msgstr "Bohaté tvrdé drevo"

#: src/ui/PicturesSheet.qml:126
#, kde-format
msgctxt "@title"
msgid "Choose a picture"
msgstr "Vybrať obrázok"

#: src/ui/PicturesSheet.qml:151
#, kde-format
msgctxt "@action:button"
msgid "Choose File…"
msgstr "Vybrať súbor..."

#: src/ui/UserDetailsPage.qml:106
#, kde-format
msgid "Change avatar"
msgstr "Zmeniť avatar"

#: src/ui/UserDetailsPage.qml:155
#, kde-format
msgid "Email address:"
msgstr "E-mailová adresa:"

#: src/ui/UserDetailsPage.qml:179
#, kde-format
msgid "Delete files"
msgstr "Vymazať súbory"

#: src/ui/UserDetailsPage.qml:186
#, kde-format
msgid "Keep files"
msgstr "Ponechať súbory"

#: src/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Delete User…"
msgstr "Vymazať používateľa..."

#: src/ui/UserDetailsPage.qml:204
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Konfigurovať overenie totožnosti odtlačkom prsta…"

#: src/ui/UserDetailsPage.qml:218
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Odtlačky prstov možno použiť namiesto hesla pri odomykaní obrazovky a "
"poskytovaní oprávnení správcu aplikáciám a programom príkazového riadka, "
"ktoré ich vyžadujú.<nl/><nl/>Prihlásenie do systému pomocou odtlačku prsta "
"zatiaľ nie je podporované."

#: src/user.cpp:279
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Nepodarilo sa získať oprávnenia na uloženie používateľa %1"

#: src/user.cpp:284
#, kde-format
msgid "There was an error while saving changes"
msgstr "Nastala chyba pri ukladaní zmien"

#: src/user.cpp:380
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr ""
"Nepodarilo sa zmeniť veľkosť obrázka: otvorenie dočasného súboru zlyhalo"

#: src/user.cpp:388
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr "Nepodarilo sa zmeniť veľkosť obrázka: zápis do dočasného súboru zlyhal"

#: src/usermodel.cpp:147
#, kde-format
msgid "Your Account"
msgstr "Vaše konto"

#: src/usermodel.cpp:147
#, kde-format
msgid "Other Accounts"
msgstr "Ostatné kontá"

#~ msgid "Continue"
#~ msgstr "Pokračovať"

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "john.doe@kde.org"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Roman Paholík"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "wizzardsk@gmail.com"

#~ msgid "Manage user accounts"
#~ msgstr "Spravovať používateľské kontá"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Carson Black"
#~ msgstr "Carson Black"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "Ján Novák"
