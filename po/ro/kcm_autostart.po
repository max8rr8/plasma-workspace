# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2008, 2009, 2010, 2012, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-01 02:34+0000\n"
"PO-Revision-Date: 2022-08-14 16:54+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: autostartmodel.cpp:320
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "„%1” nu este un URL absolut."

#: autostartmodel.cpp:323
#, kde-format
msgid "\"%1\" does not exist."
msgstr "„%1” nu există."

#: autostartmodel.cpp:326
#, kde-format
msgid "\"%1\" is not a file."
msgstr "„%1” nu este un fișier."

#: autostartmodel.cpp:329
#, kde-format
msgid "\"%1\" is not readable."
msgstr "„%1” nu poate fi citit."

#: ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr "Fă executabil"

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr "Fișierul „%1” trebuie să fie executabil pentru a rula la ieșire."

#: ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""
"Fișierul „%1” trebuie să fie executabil pentru a rula la autentificare."

#: ui/main.qml:95
#, kde-format
msgid "Properties"
msgstr "Proprietăți"

#: ui/main.qml:101
#, kde-format
msgid "Remove"
msgstr "Elimină"

#: ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr "Aplicații"

#: ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr "Scripturi pentru autentificare"

#: ui/main.qml:118
#, kde-format
msgid "Pre-startup Scripts"
msgstr "Alege script pentru pre-pornire"

#: ui/main.qml:121
#, kde-format
msgid "Logout Scripts"
msgstr "Scripturi pentru ieșire"

#: ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr "Niciun element de pornire automată specificat"

#: ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""
"Apăsați butonul <interface>Adaugă…</interface> de mai jos pentru a adăuga "
"câteva"

#: ui/main.qml:145
#, kde-format
msgid "Choose Login Script"
msgstr "Alege script pentru autentificare"

#: ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr "Alege script pentru ieșire"

#: ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr "Adaugă…"

#: ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr "Adaugă aplicație…"

#: ui/main.qml:203
#, kde-format
msgid "Add Login Script…"
msgstr "Adaugă script pentru autentificare…"

#: ui/main.qml:209
#, kde-format
msgid "Add Logout Script…"
msgstr "Adaugă script pentru ieșire…"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Sergiu Bivol"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sergiu@cip.md"

#~ msgid "Autostart"
#~ msgstr "Pornire automată"

#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Modul al panoului de control pentru Gestionarul de pornire automată"

#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "Drept de autor © 2006-2020 Echipa Gestionarului de pornire automată"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Responsabil"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Add..."
#~ msgstr "Adaugă..."

#~ msgid "Shell script path:"
#~ msgstr "Caleaa scriptului de consolă:"

#~ msgid "Create as symlink"
#~ msgstr "Creează legătură simbolică"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "Pornește automat numai în KDE"

#~ msgid "Name"
#~ msgstr "Denumire"

#~ msgid "Command"
#~ msgstr "Comandă"

#~ msgid "Status"
#~ msgstr "Stare"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Rulează la"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "Administratorul de pornire automată KDE"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Activat"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Dezactivat"

#~ msgid "Desktop File"
#~ msgstr "Fișier de birou"

#~ msgid "Script File"
#~ msgstr "Fișier-script"

#~ msgid "Add Program..."
#~ msgstr "Adăugare program..."

#~ msgid "Startup"
#~ msgstr "Demarare"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "Sunt permise numai fișierele cu extensie „.sh” pentru a configura mediul."

#~ msgid "Shutdown"
#~ msgstr "Oprire"
